const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require('next/constants')

module.exports = (phase) => {
 // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
 const isDev = phase === PHASE_DEVELOPMENT_SERVER
 // when `next build` or `npm run build` is used
 const isProd = phase === PHASE_PRODUCTION_BUILD
 
 console.log(`isDev:${isDev}  isProd:${isProd}`)

 const env = {
  RECIPE_API_HOST: (() => {
    if (isDev) return 'http://127.0.0.1:9292'
    if (isProd) return process.env.RECIPE_API_HOST
  })()
}
  
  return {
    images: {
      domains: [
        'assets.vercel.com',
        'images.ctfassets.net'
      ]
    },
    env
  }
}