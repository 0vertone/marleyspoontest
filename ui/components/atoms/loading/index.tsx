import React from 'react';
import { PixelSpinner } from 'react-epic-spinners';

type Props = {
  size?: number;
  color?: string;
  animationDuration?: number;
  className?: string;
};

const Loading: React.FC<Props & React.AllHTMLAttributes<HTMLElement>> = ({
  size,
  className,
  animationDuration,
  color,
}) => {
  return <PixelSpinner size={size} color={color} animationDuration={animationDuration} className={className} />;
};

export default React.memo(PixelSpinner);
