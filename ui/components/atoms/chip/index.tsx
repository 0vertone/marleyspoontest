import * as React from 'react';

type Props = {
  text?: string;
  className?: string;
};

const Chip: React.FC<Props & React.AllHTMLAttributes<HTMLElement>> = ({ text = '' }) => {
  return (
    <div className="flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-full text-pink-700 border border-pink-300 ">
      <div className="text-xs font-normal leading-none max-w-full flex-shrink">{text}</div>
    </div>
  );
};

export default React.memo(Chip);
