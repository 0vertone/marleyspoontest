import React from 'react';

const NavBar: React.FC<React.AllHTMLAttributes<HTMLElement>> = ({ children }) => (
  <nav className="flex items-center justify-center bg-white h-20 border-b-2">
    <div className="flex flex-shrink-0">Shane Moore</div>
  </nav>
);

export default React.memo(NavBar);
