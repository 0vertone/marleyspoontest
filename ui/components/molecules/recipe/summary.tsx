import Image from 'next/image';
import * as React from 'react';
import clsx from 'clsx';
import Link from 'next/link';
import Tilt from 'react-parallax-tilt';
import { motion } from 'framer-motion';

type Props = {
  imageUrl?: string;
  title?: string;
  recipeId?: string;
  className?: string;
};

const RecipeSummary: React.FC<Props & React.AllHTMLAttributes<HTMLElement>> = ({
  title = '',
  imageUrl = '',
  recipeId,
  className,
}) => {
  return (
    <Tilt className="border m-4 bg-white rounded">
      <div className={clsx(className, 'flex-none m-4 p-5 w-80 text-center ')}>
        <motion.figure className="image" layoutId={`recipe-image-${recipeId}`}>
          <Image
            className="rounded"
            src={imageUrl}
            layout="responsive"
            alt="Picture of a recipe"
            width={200}
            height={200}
          />
        </motion.figure>
        <motion.p className="text-xs p-4 h-20" layoutId={`recipe-title-${recipeId}`}>
          {title}
        </motion.p>
        <Link href={`/recipes/${recipeId}`}>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded align-bottom m-">
            More
          </button>
        </Link>
      </div>
    </Tilt>
  );
};

export default React.memo(RecipeSummary);
