import Image from 'next/image';
import * as React from 'react';
import parse from 'url-parse';
import clsx from 'clsx';
import { isEmpty } from 'lodash';
import Chip from '@/components/atoms/chip';

import { motion } from 'framer-motion';

export type Props = {
  recipeId?: string;
  title?: string;
  imageUrl?: string;
  tags?: Array<string>;
  chefName?: string;
  description?: string;
  className?: string;
};

export const RecipeExpanded: React.FC<Props & React.AllHTMLAttributes<HTMLElement>> = ({
  title = '',
  imageUrl = '',
  tags = [],
  chefName = 'Unknown',
  description = '',
  recipeId = '',
  className,
}) => {
  const parsedURL = parse(imageUrl);
  parsedURL.set('protocol', 'https');
  const url = parsedURL.href;

  return (
    <div>
      <div className={clsx(className, 'flex flex-row justify-center')}>
        <div className={'flex flex-col w-3/12 m-4'}>
          <motion.figure className="image" layoutId={`recipe-image-${recipeId}`}>
            <Image src={url} layout="responsive" alt="Picture of a recipe" width={300} height={200} />
          </motion.figure>
          <motion.div
            className="flex flex-wrap"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ delay: 1.0 }}
          >
            {!isEmpty(tags) &&
              tags.map((tag) => {
                return <Chip text={tag} />;
              })}
          </motion.div>
        </div>
        <div className={'flex flex-col w-3/12 m-4'}>
          <motion.p className="text-center mb-16" layoutId={`recipe-title-${recipeId}`}>
            {title}
          </motion.p>
          <motion.p initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={{ delay: 0.5 }}>
            Chef: <i>{chefName}</i>
          </motion.p>
        </div>
      </div>
      <div className={'flex flex-row justify-center'}>
        <motion.p
          className="w-6/12 text-center"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ delay: 0.2 }}
        >
          <i>{description}</i>
        </motion.p>
      </div>
    </div>
  );
};

export default RecipeExpanded;
