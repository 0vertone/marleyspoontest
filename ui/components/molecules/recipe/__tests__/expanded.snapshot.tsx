import React from 'react';
import renderer from 'react-test-renderer';
import RecipeExpanded from '../expanded';

it('renders homepage unchanged', () => {
  const tree = renderer.create(<RecipeExpanded />).toJSON();
  expect(tree).toMatchSnapshot();
});
