import React from 'react';
import NavBar from '@/molecules/navbar';

import clsx from 'clsx';

const Layout: React.FC<React.AllHTMLAttributes<HTMLElement>> = ({ children, className }) => (
  <div className="h-screen">
    <NavBar />

    <div className="bg-gradient-to-b from-white to-red-700 min-h-full">{children}</div>
  </div>
);

export default React.memo(Layout);
