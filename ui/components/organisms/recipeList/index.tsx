import React from 'react';
import RecipeSummary from '@/components/molecules/recipe/summary';
import parse from 'url-parse';

type RecipeSummary = {
  title: string;
  id: string;
  photo: {
    title: string;
    url: string;
    id: string;
  };
};

type Props = {
  recipes?: Array<RecipeSummary>;
};

const RecipeList: React.FC<Props & React.AllHTMLAttributes<HTMLElement>> = ({ recipes }) => {
  return (
    <div className="flex flex-wrap justify-center">
      {recipes?.map((summary) => {
        const {
          title,
          id,
          photo: { url },
        } = summary;
        const parsedURL = parse(url);
        parsedURL.set('protocol', 'https');
        const imageUrl = parsedURL.href;
        return <RecipeSummary title={title} imageUrl={imageUrl} recipeId={id} />;
      })}
    </div>
  );
};

export default React.memo(RecipeList);
