import React from 'react';
import Loading from '@/components/atoms/loading';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import axios from 'axios';
import useSWR from 'swr';
import { isEmpty, map } from 'lodash';
import Layout from '@/components/organisms/layout';

const RecipeExpanded = dynamic(() => import('@/components/molecules/recipe/expanded')); // Lazy-loaded

const fetcher = (url: string) => axios.get(url).then((res) => res.data);

const Loader = (): JSX.Element => {
  return (
    <div className="flex items-center justify-center h-screen">
      <Loading color="red" />
    </div>
  );
};

const RecipePage = (): JSX.Element => {
  const router = useRouter();
  const { id } = router.query;
  const { data, error } = useSWR(`/api/recipes/${id}`, fetcher);

  if (isEmpty(data)) return <Loader />;
  if (error) return <div>Failed to load recipe</div>;
  return (
    <Layout>
      <RecipeExpanded
        tags={map(data.tags, 'name')}
        imageUrl={data.photo?.url}
        recipeId={data.recipeId}
        title={data.title}
        chefName={data.chef?.name}
        description={data.description}
      />
    </Layout>
  );
};

export default RecipePage;
