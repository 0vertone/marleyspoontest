import React from 'react';
import Loading from '@/components/atoms/loading';
import dynamic from 'next/dynamic';
import axios from 'axios';
import useSWR from 'swr';
import { isEmpty } from 'lodash';
import Layout from '@/components/organisms/layout';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';

const PAGINATED_PAGE_SIZE = 10;

const RecipeList = dynamic(() => import('@/components/organisms/recipeList')); // Lazy-loaded

const fetcher = (url: string) => axios.get(url).then((res) => res.data);

const Loader = (): JSX.Element => {
  return (
    <div className="flex items-center justify-center h-screen">
      <Loading color="red" />
    </div>
  );
};

const RecipesPage: React.FC<React.AllHTMLAttributes<HTMLElement>> = () => {
  const router = useRouter();
  const currentPath = router.pathname;
  const { page } = router.query;

  const handlePageChange = (selectedItem: { selected: number }) => {
    const currentQuery = router.query;
    currentQuery.page = selectedItem.selected.toString();
    router.push({
      pathname: currentPath,
      query: currentQuery,
    });
  };

  const { data, error } = useSWR(`/api/recipes?page=${page}`, fetcher);

  if (isEmpty(data)) return <Loader />;
  if (error) return <div>Failed to load recipes</div>;

  return (
    <Layout>
      <ReactPaginate
        containerClassName="justify-center px-2 mx-4 flex"
        pageClassName="px-2 cursor-pointer"
        nextClassName="px-2 cursor-pointer"
        previousClassName="px-2 cursor-pointer"
        pageCount={data.collection_size / PAGINATED_PAGE_SIZE}
        pageRangeDisplayed={5}
        marginPagesDisplayed={2}
        onPageChange={handlePageChange}
      />
      <RecipeList recipes={data.data} />
    </Layout>
  );
};

export default RecipesPage;
