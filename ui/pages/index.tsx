import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';
import React from 'react';
import Loading from '@/components/atoms/loading';

export default function Home() {

  return (
    <div>
      <Head>
        <title>Shane Moore</title>
      </Head>

      <main className="text-center justify-center h-screen flex flex-col w-full">
        <div className="w-full justify-center my-10">
          <p className="p-4">Am I Worthy?</p>
          <Image
            priority
            loading='eager'
            className="rounded-full"
            src="/mise.jpg"
            width="500"
            height="500"
            layout="intrinsic"
            alt="Behold the power of Thor?!"
          />
        </div>
        <Link href="/recipes?page=1">
          <div className="cursor-pointer bg-red-800 text-white text-center justify-center items-center flex-grow">
            <span className="items-center m-auto flex h-full justify-center">Lets Find Out!</span>
          </div>
        </Link>
      </main>
    </div>
  );
}
