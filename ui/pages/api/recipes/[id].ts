import type { NextApiRequest, NextApiResponse } from 'next';

import { RECIPE_API_HOST } from '@/config/index';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { id },
  } = req;
  if (id == undefined) {
    // Early return for malformed requests
    return res.status(404);
  }

  const recipe_url = `${RECIPE_API_HOST}/api/v1/recipes/${id}`;
  const response = await fetch(recipe_url);
  const json = await response.json();
  res.status(200).json(json);
};
