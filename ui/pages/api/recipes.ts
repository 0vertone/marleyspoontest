import type { NextApiRequest, NextApiResponse } from 'next';

import { RECIPE_API_HOST } from '@/config/index';

type RecipeSummary = {
  photo: {
    title: string;
    file: string;
    id: string;
    url: string;
  };
  description: string;
  title: string;
  id: string;
  chef: {
    name: string;
    id: string;
  };
  calories: number;
};

export default async (req: NextApiRequest, res: NextApiResponse<RecipeSummary | any>) => {
  const cursor: number = +req.query.page - 1; // A cursor starts from 0 whereas a page starts from 1.
  const recipe_url = `${RECIPE_API_HOST}/api/v1/recipes?cursor=${cursor}`;
  const response = await fetch(recipe_url);
  const json = await response.json();
  res.status(200).json(json);
};
