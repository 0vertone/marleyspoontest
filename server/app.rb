# frozen_string_literal: true

VERSION = 1.0

ENV['ROOT_PATH'] = File.dirname(__FILE__)
ROOT = ENV['ROOT_PATH']
ENV['RACK_ENV'] ||= 'development'
RACK_ENV = ENV['RACK_ENV'].to_s.to_sym
ENV_TEST = RACK_ENV.eql?(:test)
ENV_DEVELOPMENT = RACK_ENV.eql?(:development)
ENV_PRODUCTION = RACK_ENV.eql?(:production)

require 'sinatra'
require 'sinatra/namespace'
require 'sinatra/json'
require 'sinatra/reloader' if ENV_DEVELOPMENT
require_relative 'config/logger'

[
  "#{ROOT}/config/initializers/**/*.rb",
  "#{ROOT}/db/**/*.rb"
].each do |dir|
  Dir[dir].each do |file|
    # TODO: fix the damn logger
    # logger.info("loading #{file}")
    require_relative file
  end
end

class MarleySpoonApp < Sinatra::Base
  register Sinatra::Namespace

  set :environment, RACK_ENV
  set :server, 'thin'

  include Logging

  configure :development do
    register Sinatra::Reloader
  end

  before do
    content_type 'application/json'

    @client ||= Contentful::Client.new(
      space: ENV['CONTENTFUL_SPACE_ID'],
      access_token: ENV['CONTENTFUL_ACCESS_TOKEN'],
      dynamic_entries: :auto,
      entry_mapping: {
        ContentType::TAG => CustomSerializableEntry,
        ContentType::RECIPE => CustomSerializableEntry,
        ContentType::CHEF => CustomSerializableEntry
      },
      resource_mapping: {
        'Asset' => CustomSerializableAsset
      }
    )
  end

  not_found do
    status 404
    body json(message: 'not found')
  end

  error do
    exception = env['sinatra.error']
    @logger.info(exception)
    status 500
    body json(message: exception)
  end

  namespace '/api' do
    namespace '/v1' do
      get '/recipes' do
        cursor = params[:cursor].to_i || 0
        entries = @client.entries(
          content_type: ContentType::RECIPE,
          include: 2,
          skip: cursor * CONTENTFUL_BASE_LIMIT,
          limit: CONTENTFUL_BASE_LIMIT
        )

        status 200
        body json({ data: entries.map(&:to_hash), collection_size: entries.total })
      end

      get '/recipes/:id' do
        entry = @client.entry(params[:id])

        status 200
        body json(entry.to_hash)
      end
    end
  end
end
