# frozen_string_literal: true

require 'dotenv'
Dotenv.load('.env.local')

require_relative 'config/environment'
require_relative 'app'

run MarleySpoonApp
