# frozen_string_literal: true

require 'logger'

module Logging
  def logger
    Logging.logger
  end

  def self.logger
    @logger ||= if 'production'.eql?(ENV['RACK_ENV'])
                  ::Logger.new($stdout)
                else
                  ::Logger.new("#{File.dirname(__FILE__)}/../log/#{ENV['RACK_ENV']}.log")
                end
    @logger.level = Logger::DEBUG
    @logger
  end
end
