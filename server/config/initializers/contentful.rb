# frozen_string_literal: true

require 'contentful'
require_relative 'serializers'

CONTENTFUL_BASE_LIMIT = 10

module ContentType
  RECIPE = 'recipe'
  CHEF = 'chef'
  TAG = 'tag'

  PAGE = 'page'
  STATIC_PAGE = 'staticPage'
  FOOTER_SECTION = 'faqSection'
  FOOTER_SECTION_ITEM = 'faqSection'
  SETTINGS = 'settings'
end
