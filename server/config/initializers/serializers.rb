# frozen_string_literal: true

require 'contentful'

module Serializable
  def to_hash
    fields.merge(id: id)
  end

  def to_json(**_args)
    JSON.dump(to_hash)
  end
end

class CustomSerializableEntry < Contentful::Entry
  include Serializable
end

class CustomSerializableAsset < Contentful::Asset
  include Serializable

  def to_hash
    super.merge(url: url)
  end
end
