# frozen_string_literal: true

# With more time, one could mock out the entire contentful API and have very snappy tests.
def stub_authorization(space_id)
  stub_request(:any, "https://cdn.contentful.com/spaces/#{space_id}/environments/master/content_types?limit=1000")
    .to_return(status: 200, body: '', headers: {})
end
