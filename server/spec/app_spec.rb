# frozen_string_literal: true

require_relative './http_mocks/contentful'

RSpec.describe MarleySpoonApp do
  def app
    @app ||= MarleySpoonApp # this defines the active application for this test
  end

  before(:each) do
    # In a production application, having real network requests is bad practice and makes them indeterminate.
    # There are two approaches to resolving this. Mocking the methods used by the client individually
    # or intercepting the network requests and having pre prepared data returned.

    # I prefer the latter as it provides a cleaner upgrade path and tests more of the code.
    # Real network testing should normally be left to integration or E2E tests.

    # stub_authorization(ENV['SPACE_ID'])

    # Never normally commit commented code, but this is how one would stub a url.
    # This test file is an example of how I prefer to structure my behavioral tests
  end

  describe 'GET /api/v1/recipes' do
    context 'with real HTTP requests' do
      let(:parsed) { JSON.parse(response.body) }
      let(:data) { parsed['data'] }
      let(:first_recipe) { data[0] }

      before(:each) do
        WebMock.disable!
      end

      context 'with no params' do
        let(:response) { get '/api/v1/recipes' }

        it 'returns status 200' do
          expect(response.status).to eq 200
        end

        it 'returns a JSON body' do
          expect(parsed).to be_a Hash
          expect(response.status).to eq 200
        end

        it 'returns a data key in the hash with an array value' do
          expect(parsed).to have_key('data')
          expect(data).to be_a Array
        end

        it 'returns at least one result' do
          expect(data.count.positive?).to be_truthy
        end

        it 'returns the correct recipe structure' do
          expect(first_recipe.keys.sort).to eq(%w[calories description id photo tags title])
        end
      end

      context 'with cursor' do
        let(:response) { get '/api/v1/recipes?cursor=10' }
        it 'will return the next set of results' do
          # Fragile test since at the moment, only 4 results are in the contentful DB.
          # Once stubbed, this test would change to ensure the first response and second contain different results
          expect(data).to be_empty
        end
      end
    end
  end

  describe 'GET /api/v1/recipes/:id' do
    context 'with real HTTP requests' do
      let(:parsed) { JSON.parse(response.body) }
      let(:data) { parsed['data'] }
      let(:first_recipe) { data.first }

      before(:each) do
        WebMock.disable!
      end

      context 'with no params' do
        let(:response) { get '/api/v1/recipes/4dT8tcb6ukGSIg2YyuGEOm' }

        it 'will return a recipe' do
          # A fragile test since I have no factory for recipes or HTTP stubs.
          expect(parsed.keys.sort).to eq(%w[calories description id photo tags title])
        end
      end
    end
  end
end
